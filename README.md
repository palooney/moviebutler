# Movie Butler

## Introduction

This is a small personalized movie recommendation written by Python. The dataset for this repo is collected from [MovieLens](https://grouplens.org/datasets/movielens/), which is created and maintained by [Grouplens](https://grouplens.org/about/what-is-grouplens/), a research lab in the Department of Computer Science and Engineering at University of Minnesota, Twin Cities specializing in recommender systems, online communities, mobile and ubiquitous technologies, digital libraries, and local geographic information systems.

This project is inspired by a very terrible [Douban](https://www.douban.com/) book recommendation.

## Data Introduction

The dataset for this repo can be found at [dataset](https://grouplens.org/datasets/movielens/), under *recommended for education and development*. In this repo, the main dataset is the *small* dataset which contains 100,000 ratings and 1,00 tag applications applied to 9000 movies by 700 users.

### Cication

F. Maxwell Harper and Joseph A. Konstan. 2015. The MovieLens Datasets: History and Context. ACM Transactions on Interactive Intelligent Systems (TiiS) 5, 4, Article 19 (December 2015), 19 pages. DOI=<http://dx.doi.org/10.1145/2827872>

## Features

- Making movie recommendation based on user historical data.

