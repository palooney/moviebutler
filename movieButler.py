## import molude
import csv
from collections import defaultdict

## read movie dataset
## TODO: need to change ID type
def readMovie(fileName):
	movieID = {}
	with open(fileName, 'r') as f: ## only reading
		next(f, None) # pass header
		data = csv.reader(f, delimiter = ',')
		movieID = dict(row[:2] for row in data)
	return movieID

## get tag of the movie
## TODO: need to change ID type
def movieTag(fileName):
	tags = {}
	with open(fileName, 'r') as f:
		next(f, None)
		data = csv.reader(f, delimiter = ',')
		tags = dict([row[0], row[-1]] for row in data)
	for key, value in tags.items():
		key = int(key)
		tags[key] = value.split('|')
	return tags

## get ratings of all movies
def ratings(fileName):
	rates = {}
	with open(fileName, 'r') as f:
		next(f, None)
		data = csv.reader(f, delimiter = ',')
		for line in data:
			if int(line[1]) in rates:
				rates[int(line[1])].append(float(line[2]))
			else:
				rates[int(line[1])] = [float(line[2])]
	return rates
## get tags of all movies
def review(fileName):
	reviews = defaultdict(list)
	with open(fileName, 'r') as f:
		next(f, None)
		data = csv.reader(f, delimiter = ',')
		for line in data:
			if int(line[1]) in reviews:
				reviews[int(line[1])].append(line[2])
			else:
				reviews[int(line[1])] = [line[2]]
	return reviews

## testing
movieID = readMovie('movies.csv')
tags = movieTag('movies.csv')
rates = ratings('ratings.csv')
reviews = review('tags.csv')
print reviews